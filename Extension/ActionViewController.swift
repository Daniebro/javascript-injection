//
//  ActionViewController.swift
//  Extension
//
//  Created by Danni Brito on 2/11/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit
import MobileCoreServices

class ActionViewController: UIViewController {
    
    @IBOutlet var script: UITextView!
    
    var pageTitle = ""
    var pageURL = ""
    var dict = [String: String]()
    var allScripts = [String: String]()
    
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        dict = defaults.object(forKey: "ScriptsByUrl") as? [String: String] ?? [String: String]()
        allScripts = defaults.object(forKey: "AllScripts") as? [String: String] ?? [String: String]()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(selectPreview))
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let load = UIBarButtonItem(title: "Load", style: .plain, target: self, action: #selector(selectScript))
        let save = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveScript))
        
        toolbarItems = [load,spacer, save]
        navigationController?.setToolbarHidden(false, animated: false)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        if let inputItem = extensionContext?.inputItems.first as? NSExtensionItem {
            if let itemProvider = inputItem.attachments?.first {
                itemProvider.loadItem(forTypeIdentifier: kUTTypePropertyList as String) {
                    [weak self] (dict, error) in
                    guard let itemDictionary = dict as? NSDictionary else { return }
                    guard let javaScriptValues = itemDictionary[NSExtensionJavaScriptPreprocessingResultsKey] as? NSDictionary else { return }
                    self?.pageTitle = javaScriptValues["title"] as? String ?? ""
                    self?.pageURL = URL(string:javaScriptValues["URL"] as? String ?? "")?.host ?? ""
                    
                    var showedScript = ""
                    for (url, urlScript) in self!.dict {
                        if url == self?.pageURL {
                            showedScript = urlScript
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self?.title = self?.pageTitle
                        self?.script.text = showedScript
                    }
                }
            }
        }
       
    }

    @IBAction func done() {
        let defaults = UserDefaults.standard
        let item = NSExtensionItem()
        let argument: NSDictionary = ["customJavaScript" : script.text ?? ""]
        let webDictionary: NSDictionary = [NSExtensionJavaScriptFinalizeArgumentKey: argument]
        let customJavaScript = NSItemProvider(item: webDictionary, typeIdentifier: kUTTypePropertyList as String)
        item.attachments = [customJavaScript]
        extensionContext?.completeRequest(returningItems: [item])
        dict[pageURL] = script.text
        defaults.set(dict, forKey: "ScriptsByUrl")
        defaults.set(allScripts, forKey: "AllScripts")
        
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        
        let keabordScreenEndFram = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keabordScreenEndFram, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            script.contentInset = .zero
            
        } else {
            script.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        script.scrollIndicatorInsets = script.contentInset
        
        let selectedRange = script.selectedRange
        script.scrollRangeToVisible(selectedRange)
    }
    
    @objc func selectPreview(){
        let ac = UIAlertController(title: "Code Previews", message: "Select one of our code previews", preferredStyle: .actionSheet)
        ac.addAction(UIAlertAction(title: "SimpleAlert", style: .default, handler: { (_) in
            self.script.text = "alert(\"hello\");"
        }))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(ac, animated: true)
        
    }
    
    @objc func selectScript() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ScriptsTable") as? ScriptsTableViewController {
            print(allScripts)
            vc.delegate = self
            vc.allScripts = allScripts
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func saveScript() {
        let ac = UIAlertController(title: "Save new script", message: "Insert the name", preferredStyle: .alert)
        ac.addTextField(configurationHandler: nil)
        ac.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak self] (_) in
            self?.allScripts[ac.textFields![0].text!] = self?.script.text
            self?.defaults.set(self?.allScripts, forKey: "AllScripts")
        }))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(ac, animated: true, completion: nil)
        
    }

}

extension UITextView {
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
