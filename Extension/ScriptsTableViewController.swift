//
//  ScriptsTableViewController.swift
//  Extension
//
//  Created by Danni Brito on 6/1/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit

class ScriptsTableViewController: UITableViewController {
    
    var allScripts: [String: String]!
    weak var delegate: ActionViewController!
    var titles = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(allScripts)
        titles = allScripts.map { $0.key }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScriptCell", for: indexPath)

        cell.textLabel?.text = titles[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.script.text = allScripts[titles[indexPath.row]]
        navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            allScripts[titles[indexPath.row]] = nil
            titles.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            delegate.defaults.set(allScripts, forKey: "AllScripts")
            delegate.allScripts = delegate.defaults.object(forKey: "AllScripts") as? [String: String] ?? [String: String]()
        }
    }

}
